const APIURL = 'http://dataservice.accuweather.com/locations/v1/postalcodes/search?apikey=';
const APIURL2 = '&q=';
const APIURL3 = '&language=en-us&details=true';
const APIURL4 = 'http://dataservice.accuweather.com/forecasts/v1/daily/5day/';
const APIURL5 = '?apikey=';
const APIURL6 = '&language=en';

class Forecast {
  constructor(ctry, state, city) {  // Constructor
    this.country = ctry;
	this.state = state;
	this.city = city;
	}
	
	toString() {
		return this.city + ", " + this.state + " " + this.country;
  }
}


let locationKey = function(){

	let postalcodeValue = localStorage.getItem('postalcodeValue');
	let url = APIURL + API_KEY + APIURL2 + postalcodeValue + APIURL3;
	
	fetch(url)
.then(response => {
	//console.log(response);
	return response.json();
	}).then(data => {
	let lk = data[0].Details.Key;
	let ctry = data[0].Country.LocalizedName;
	let state = data[0].AdministrativeArea.LocalizedName;
	let city = data[0].EnglishName;
	getconditions(lk, ctry, state, city);
	});
	
};

let getconditions = function(lk, ctry, st, city){

	const f = new Forecast(ctry, st, city);
	const fstring = "5 Day Forecast for " + f.toString() + ":";
	const loc = document.getElementById('loc');
	loc.innerHTML = `
<h2>${fstring}</h2>
<br />
`;
	
	
	let url2 = APIURL4 + lk + APIURL5 + API_KEY + APIURL6;
	fetch(url2)
.then(response => {
	//console.log(response);
	return response.json();
	}).then(data => {
		
	for(let i=0; i < 5; i++){
		
		const date = data.DailyForecasts[i].Date;
		const icon = data.DailyForecasts[i].Day.IconPhrase;
		const maxtemp = data.DailyForecasts[i].Temperature.Maximum.Value;
		const mintemp = data.DailyForecasts[i].Temperature.Minimum.Value;
		const metrics = data.DailyForecasts[i].Temperature.Minimum.Unit;
		const element = document.getElementById('article-title' + i);
		element.innerHTML = `
<h3>${date}</h3>
<p>${icon}</p>
<p>${maxtemp} ${metrics}</p>
<p>${mintemp} ${metrics}</p>
<br />
<br />
`;
	}

	});
	
};



// TODO
let getValidity = function(){

	let initial = false;
	const sm = document.getElementById('search-method');
	if(sm.value === 'choose'){
		initial = true;
		sm.classList.add('invalid');
		document.getElementById('searchmethodsmall').innerText = "Error: Must choose to search by city and country or by US postal code.";
	}
	else{
		sm.classList.remove('invalid');
		sm.classList.add('valid');
		document.getElementById('csitesmall').innerText = '';
	}
	return initial;
	
};

let getHiddenValidity = function(){
	let hiddenDiv = document.getElementById('hidden_div');
	let initial = false;
	if (hiddenDiv.style.display === 'block'){

		if(document.getElementById('search-method').value === "textsearch"){
		
			let citytext = document.getElementById('jtitle');
			let cSite = document.getElementById('csite');

			if(citytext.value.length === 0){
				initial = true;
				citytext.classList.add('invalid');
				//console.log(citytext.validationMessage);
				document.getElementById('jtitlesmall').innerText = "Error: Must enter a city name in text.";
			}
			else{
			citytext.classList.remove('invalid');
			citytext.classList.add('valid');
			document.getElementById('jtitlesmall').innerText = '';
			}
			if(cSite.value === 'choose'){
				initial = true;
				cSite.classList.add('invalid');
				document.getElementById('csitesmall').innerText = "Error: Must select a country.";
			}
			else{
				cSite.classList.remove('invalid');
				cSite.classList.add('valid');
				document.getElementById('csitesmall').innerText = '';
			}
		}
			
		
	
	if(document.getElementById('search-method').value === 'postalsearch'){
		const postal = document.getElementById("codelanguage");
		let reg = /[0-9]{5}/
		if( (postal.value.length === 0)||(!(reg.test(postal.value)))){
				initial = true;
				postal.classList.add('invalid');
				document.getElementById('hiddencodesall').innerText = "Error: Must enter a valid postal code (5 digits).";
			
			}
			else{
				postal.classList.remove('invalid');
				postal.classList.add('valid');
				document.getElementById('hiddencodesall').innerText = '';
			}
		}
	}
	return initial;
};

let showDiv = function showDiv(divId, element)
{
	document.getElementById(divId).style.display = 'block';

	if(element.value === "textsearch"){
		document.getElementById('hidden_text').style.display = 'block';
		document.getElementById('hidden_postal').style.display = 'none';
	}
	if(element.value === "postalsearch"){
		document.getElementById('hidden_postal').style.display = 'block';
		document.getElementById('hidden_text').style.display = 'none';
	}
}

const form = document.getElementById('connect-form');


form.addEventListener("submit", function(e){

	let formIsValid = true;
	
	let rets = getValidity();	
	let ret = getHiddenValidity();
		
	if(ret || rets){
		formIsValid = false;
	}
	else{
			e.preventDefault();
let countryValue = document.getElementById("csite").value;
	let postalcodeValue = document.getElementById("codelanguage").value;
	let cityValue = document.getElementById("jtitle").value;
	localStorage.setItem('countryValue', countryValue);
	localStorage.setItem('postalcodeValue', postalcodeValue);
	localStorage.setItem('cityValue', cityValue);
	
	let stuff = locationKey();
}	
		if(!formIsValid){
		e.preventDefault();
		console.log("Bad input");
		}
	
});
	
